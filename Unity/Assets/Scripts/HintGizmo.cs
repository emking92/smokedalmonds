﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class HintGizmo : MonoBehaviour 
{

	#region Fields

    public Arrows arrow;

    private static readonly Dictionary<Arrows, string> ARROW_TEXT = new Dictionary<Arrows,string>()
    {
        {Arrows.Up, "↑"},
        {Arrows.Down, "↓"},
        {Arrows.Right, "→"},
        {Arrows.Left, "←"},
        {Arrows.None, "■"}
    };
	
	#endregion
	
	
	#region Constants
	
	
	
	#endregion
	
	
	#region Properties
	
	
	
	#endregion
	

	#region Behaviours

	private void Awake()
	{
		
	}
	
	private void Start() 
	{
		
	}	
	
	private void Update() 
	{
		
	}

    private void OnDrawGizmos()
    {
        Gizmos2.DrawLabel(transform.position, ARROW_TEXT[arrow], Color.cyan);
    }
	
	#endregion
	
	
	#region Public Methods
	
	
	
	#endregion
	
	
	#region Private Methods
	
	
	
	#endregion
	
}

public enum Arrows
{
    Up,
    Down,
    Left,
    Right,
    None,
}
