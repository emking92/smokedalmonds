﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[RequireComponent(typeof(ResettablePosition))]
[RequireComponent(typeof(Collider2D))]
public class RocketSpike : MonoBehaviour, IResettable, IRandomizable
{

	#region Fields

    public float proximityTrigger = .5f;
    public float velocity = 5;

    private bool isTriggered;
	
	#endregion
	
	
	#region Constants
	
	
	
	#endregion
	
	
	#region Properties
	
	
	
	#endregion
	

	#region Behaviours
	
	private void Update() 
	{
        if (isTriggered)
        {
            transform.position += Vector3.up * velocity * Timekeeper.DeltaTimeMain;
        }
        else
        {
            if (transform.position.x < Player.CurrentLocation.x + proximityTrigger)
            {
                isTriggered = true;
                GetComponent<Animator>().SetBool("Fly", true);
                GetComponent<AudioSource>().Play();
            }
        }
	}
	
	#endregion
	
	
	#region Public Methods

    public void Randomize(int rand)
    {
        if (rand % 2 == 0)
        {
            this.enabled = false;
        }
    }

    public void ResetState()
    {
        isTriggered = false;
        GetComponent<Animator>().SetBool("Fly", false);
    }	
	
	#endregion
	
	
	#region Private Methods
	
	
	
	#endregion

}
