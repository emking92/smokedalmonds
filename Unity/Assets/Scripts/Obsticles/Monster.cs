﻿using UnityEngine;
using System.Collections;

public class Monster : MonoBehaviour, IResettable, IRandomizable {
   
    public float proximityTrigger = .5f;
    public bool isJumper;

    private GameObject DeadlyBumper;
    private bool IsAlive = true;
    private bool triggered;
    private Animator animator;
    private float deathTimer;
    private bool dying;
    private Collider2D[] colliders;
    private float jumpTimer;


    // Use this for initialization
	void Awake () {
        DeadlyBumper = transform.FindChild("DeadlyBumpers").gameObject;
        animator = GetComponent<Animator>();
        colliders = GetComponentsInChildren<Collider2D>();

        GetComponent<PhysicsCheck>().jump = 9;
	}

	
	// Update is called once per frame
	void Update () {
        if (!IsAlive) return;

        if (isJumper && !triggered && transform.position.x < Player.CurrentLocation.x + proximityTrigger)
        {
            triggered = true;
           
            GetComponent<PhysicsCheck>().setJump();
            GetComponents<AudioSource>()[0].Play();
            animator.SetBool("IsJumping", true);
            jumpTimer = .5f;
        }

        if (jumpTimer > 0)
        {
            jumpTimer -= Timekeeper.DeltaTimeMain;
            if (jumpTimer <= 0)
            {
                animator.SetBool("IsJumping", false);
            }
        }

        DeadlyBumper.SetActive(isJumper || !Player.Current.IsKicking);

        if (dying)
        {
            deathTimer -= Timekeeper.DeltaTimeMain;
            if (deathTimer <= 0)
            {
                SetAlive(false);
            }
            DeadlyBumper.SetActive(false);
        }
	}

    public void ResetState() {
        triggered = false;
        GetComponent<PhysicsCheck>().setVelocity(Vector2.zero);
        animator.SetBool("IsDead", false);
        animator.SetBool("IsJumping", false);
        dying = false;
        deathTimer = 0;
        SetAlive(true);
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
       
        if (!isJumper && collision.gameObject.tag == "Player" && Player.Current.IsKicking && !dying)
        {
            GetComponents<AudioSource>()[1].Play();
            dying = true;
            deathTimer = .5f;
            animator.SetBool("IsDead", true);
            foreach (Collider2D c in colliders)
            {
                c.enabled = false;
            }
            GetComponentInChildren<PhysicsCheck>().enabled = false;
        }
    }

    private void SetAlive(bool Alive)
    {
        IsAlive = Alive;
        GetComponent<SpriteRenderer>().enabled = Alive;
        foreach (Collider2D c in colliders)
        {
            c.enabled = Alive;
        }
        GetComponentInChildren<PhysicsCheck>().enabled = Alive;
    }

    public void Randomize(int rand)
    {
        isJumper = (rand % 2 == 0);
    }
}