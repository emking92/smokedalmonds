﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class FallPad : MonoBehaviour, IResettable
{

	#region Fields

    private bool isOpen;
    private BoxCollider2D floorCollider;
    private SpriteRenderer spriteRenderer;
	
	#endregion
	

	#region Behaviours

	private void Awake()
	{
        floorCollider = GetComponent<BoxCollider2D>();
        spriteRenderer = GetComponent<SpriteRenderer>();
	}

    private void OnTriggerStay2D(Collider2D collider)
    {
        if (collider.tag == "Player" && !isOpen && Player.Current.IsSliding)
        {
            isOpen = true;
            floorCollider.enabled = false;
            GetComponent<Animator>().SetBool("Open", true);
            GetComponent<AudioSource>().Play();
        }
    }
	
	#endregion
	
	
	#region Public Methods

    public void ResetState()
    {
        isOpen = false;
        floorCollider.enabled = true;
        GetComponent<Animator>().SetBool("Open", false);
        GetComponent<AudioSource>().Stop();
    }
	
	#endregion
	
}
