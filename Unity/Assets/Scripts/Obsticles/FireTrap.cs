﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class FireTrap : MonoBehaviour, IRandomizable
{

	#region Fields
	
	
	
	#endregion
	
	
	#region Constants
	
	
	
	#endregion
	
	
	#region Properties
	
	
	
	#endregion
	

	#region Behaviours

	private void Awake()
	{
		
	}
	
	private void Start() 
	{
		
	}	
	
	private void Update() 
	{
		
	}
	
	#endregion
	
	
	#region Public Methods

    public void Randomize(int rand)
    {
        if (rand % 2 == 0)
        {
            transform.FindChild("FireDown").gameObject.SetActive(false);
        }
        else
        {
            transform.FindChild("FireUp").gameObject.SetActive(false);
        }
    }	
	
	#endregion



}
