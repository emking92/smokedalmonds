﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[RequireComponent(typeof(ResettablePosition))]
[RequireComponent(typeof(Collider2D))]
public class Guillotine : MonoBehaviour, IResettable, IRandomizable
{

	#region Fields

    public float proximityTrigger = .5f;
    public float velocity = 5;
    public float minY;

    private bool isTriggered;
    private float startY;
	
	#endregion
	
	
	#region Constants
	
	
	
	#endregion
	
	
	#region Properties
	
	
	
	#endregion
	

	#region Behaviours

	private void Awake()
	{
		
	}
	
	private void Start() 
	{
        startY = transform.localPosition.y;
	}	
	
	private void Update() 
	{
        if (isTriggered)
        {
            float y = -1 + (transform.position.x - Player.CurrentLocation.x) * ((startY + 1)/proximityTrigger);
            if (y < minY) y = minY;
            if (!Player.Current.IsAlive) y = minY;

            transform.localPosition = new Vector2(0f, y);
        }
        else if (transform.position.x < Player.CurrentLocation.x + proximityTrigger)
        {
            isTriggered = true;
            transform.parent.Find("Monster").GetComponent<Animator>().SetBool("Trigger", true);
            GetComponent<AudioSource>().Play();
        }
        else if (transform.position.x < Player.CurrentLocation.x + proximityTrigger + 2)
        {
            transform.parent.Find("Monster").GetComponent<Animator>().SetBool("Trigger", true);
        }
	}
	
	#endregion
	
	
	#region Public Methods

    public void ResetState()
    {
        isTriggered = false;
        transform.parent.Find("Monster").GetComponent<Animator>().SetBool("Trigger", false);
    }
    
    public void Randomize(int rand)
    {
        if (rand % 2 == 0)
        {
            minY = .5f;
        }
    }
	
	#endregion
	
	
	#region Private Methods
	
	
	
	#endregion

}
