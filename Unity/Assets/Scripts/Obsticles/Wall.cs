﻿using UnityEngine;
using System.Collections;

public class Wall : MonoBehaviour, IResettable {
   
    public float proximityTrigger = .5f;
    public bool isJumper;
    private GameObject DeadlyBumper;
    private bool IsAlive = true;
    private bool triggered;

    // Use this for initialization
	void Awake () {
        DeadlyBumper = transform.FindChild("DeadlyBumpers").gameObject;
	}
	
	// Update is called once per frame
	void Update () {
        if (!IsAlive) return;

        if (isJumper && !triggered && transform.position.x < Player.CurrentLocation.x + proximityTrigger)
        {
            triggered = true;
            GetComponent<PhysicsCheck>().setJump(); 
        }
      

        DeadlyBumper.SetActive(!isJumper || !Player.Current.IsKicking);
	}

    public void ResetState() {
        triggered = false;
        GetComponent<PhysicsCheck>().setVelocity(Vector2.zero);
        SetAlive(true);
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
       
        if (isJumper && collision.gameObject.tag == "Player" && Player.Current.IsKicking)
        {
            SetAlive(false);

        }
    }

    private void SetAlive(bool Alive)
    {
        IsAlive = Alive;
        GetComponent<SpriteRenderer>().enabled = Alive;
        GetComponentsInChildren<Collider2D>().Foreach(f => f.enabled = Alive);
        GetComponentInChildren<PhysicsCheck>().enabled = Alive;
        transform.parent.GetComponent<SpriteRenderer>().enabled = Alive;
    }
}
