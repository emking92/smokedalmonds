﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public interface IRandomizable
{
    void Randomize(int rand);
}
