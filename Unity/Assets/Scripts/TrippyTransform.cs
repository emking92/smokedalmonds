﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TrippyTransform : MonoBehaviour 
{

	#region Fields

    public float MaxRotationSpeed;
    public float RotationLerpSpeed;
    public float maxRotationDuration = 3f;

    public float StretchRadius;
    public float StretchLerpSpeed;
    public float maxScaleDuration = 3f;

    public float TranslateRadius;
    public float TranslateLerpSpeed;
    public float MaxTranslateDuration = 3f;

    private float rotationSpeed;
    private float targetRotationSpeed;
    private float rotationTimer;

    private Vector2 startScale;
    private Vector2 targetScale;
    private float scaleTimer;

    private Vector3 startPosition;
    private Vector3 targetPosition;
    private float translateTimer;

	#endregion
	
	
	#region Constants
	
	
	
	#endregion
	
	
	#region Properties
	
	
	
	#endregion
	

	#region Behaviours

	private void Awake()
	{
        startScale = transform.localScale;
        startPosition = transform.localPosition;
	}
	
	private void Start() 
	{
		
	}	
	
	private void Update() 
	{
        rotationTimer -= Timekeeper.DeltaTimeMain;
        if (rotationTimer <= 0f)
        {
            targetRotationSpeed = UnityEngine.Random.Range(-MaxRotationSpeed, MaxRotationSpeed);
            rotationTimer = UnityEngine.Random.Range(1, maxRotationDuration);
        }
        rotationSpeed = Mathf.Lerp(rotationSpeed, targetRotationSpeed, RotationLerpSpeed * Timekeeper.DeltaTimeMain);
        transform.Rotate2D(rotationSpeed * Timekeeper.DeltaTimeMain);


        scaleTimer -= Timekeeper.DeltaTimeMain;
        if (scaleTimer <= 0f)
        {
            targetScale = startScale + UnityEngine.Random.insideUnitCircle * StretchRadius;
            scaleTimer = UnityEngine.Random.Range(1, maxScaleDuration);
        }
        transform.localScale = Vector2.Lerp(transform.localScale, targetScale, StretchLerpSpeed* Timekeeper.DeltaTimeMain);


        translateTimer -= Timekeeper.DeltaTimeMain;
        if (translateTimer <= 0f)
        {
            targetPosition = startPosition + (Vector3) UnityEngine.Random.insideUnitCircle * TranslateRadius;
            translateTimer = UnityEngine.Random.Range(1, MaxTranslateDuration);
        }
        transform.localPosition = Vector3.Lerp(transform.localPosition, targetPosition, TranslateLerpSpeed * Timekeeper.DeltaTimeMain);

	}
	
	#endregion
	
	
	#region Public Methods
	
	
	
	#endregion
	
	
	#region Private Methods
	
	
	
	#endregion
	
}
