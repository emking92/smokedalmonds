﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class DeathHUD : MonoBehaviour 
{

	#region Fields
	
	
	
	#endregion
	
	
	#region Constants
	
	
	
	#endregion
	
	
	#region Properties

    public static DeathHUD Current
    {
        get;
        private set;
    }
	
	#endregion
	

	#region Behaviours

	private void Awake()
	{
        Current = this;
	}
	#endregion
	
	
	#region Public Methods
	
	
	
	#endregion
	
	
	#region Private Methods
	
	
	
	#endregion
	
}
