﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ParticleShower : MonoBehaviour 
{

	#region Fields

    public GameObject ParticlePrefab;

    public float Width;
    public float Distance;
    public float Depth;
    public float SpawnRate;
    public float ParticleSpeed;
    public float MinScale;
    public float MaxScale;

    private float spawnTimer;
	
	#endregion
	
	
	#region Constants
	
	
	
	#endregion
	
	
	#region Properties
	
	
	
	#endregion
	

	#region Behaviours

	private void Awake()
	{
		
	}
	
	private void Start() 
	{
		
	}	
	
	private void Update() 
	{
        spawnTimer -= Timekeeper.DeltaTimeMain;

        while (spawnTimer < 0)
        {
            spawnTimer += SpawnRate;

            Vector3 position = transform.position
                + transform.right * UnityEngine.Random.Range(-Width / 2, Width / 2)
                + transform.forward * UnityEngine.Random.Range(-Depth / 2, Depth / 2);
            GameObject particle = GameObject.Instantiate(ParticlePrefab, position, transform.rotation) as GameObject;
            particle.GetComponent<ParticleObject>().Init(ParticleSpeed, Distance);
            particle.transform.localScale = Vector3.one * UnityEngine.Random.Range(MinScale, MaxScale);
        }
	}

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;

        Gizmos.DrawLine(transform.position - transform.right * Width / 2, transform.position + transform.right * Width / 2);
        Gizmos.DrawLine(transform.position, transform.position+ transform.up * Distance);
        Gizmos.DrawLine(transform.position - transform.forward * Depth / 2, transform.position + transform.forward * Depth / 2);
    }

	#endregion
	
	
	#region Public Methods
	
	
	
	#endregion
	
	
	#region Private Methods
	
	
	
	#endregion
	
}
