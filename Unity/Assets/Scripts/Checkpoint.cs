﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Checkpoint : MonoBehaviour 
{

	#region Fields

    private bool triggered;
	
	#endregion
	
	
	#region Constants
	
	
	
	#endregion
	
	
	#region Properties

    public GameObject Chunk
    {
        get;
        private set;
    }
	
	#endregion
	

	#region Behaviours

    private void Start()
    {
        Chunk = transform.parent.gameObject;
    }

	#endregion
	
	
	#region Public Methods

    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (!triggered && collider.gameObject.tag == "Player")
        {
            collider.SendMessage("OnCheckpoint", this);
            GetComponent<Animator>().SetTrigger("Trigger");
            triggered = true;
        }
    }
	
	#endregion
	
	
	#region Private Methods
	
	
	
	#endregion
	
}
