﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PlayerExplosion : MonoBehaviour 
{

	#region Fields

    public float TTL = 60f;
    private float timer;
	
	#endregion
	
	
	#region Constants

	
	#endregion
	
	
	#region Properties
	
	
	
	#endregion
	

	#region Behaviours

	private void Awake()
	{
		
	}
	
	private void Start() 
	{
        GetComponentsInChildren<Rigidbody2D>().Foreach(r =>
            {
                r.AddForce(UnityEngine.Random.insideUnitCircle * 150 + new Vector2(1, -1) *50);
                r.AddTorque(UnityEngine.Random.Range(-.5f, .5f));
            });

        timer = TTL;
	}	
	
	private void Update() 
	{
        timer -= Timekeeper.DeltaTimeMain;

        if (timer < 0)
        {
            Destroy(gameObject);
        }
	}
	
	#endregion
	
	
	#region Public Methods
	
	
	
	#endregion
	
	
	#region Private Methods
	
	
	
	#endregion
	
}
