﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ParticleObject : MonoBehaviour 
{

	#region Fields

    private float speed;
    private float maxDistanceSqr;

    private Vector3 spawn;
	
	#endregion
	

	#region Behaviours

	private void Awake()
	{
        spawn = transform.position;
	}
	
	private void Update() 
	{
		if ((transform.position - spawn).sqrMagnitude > maxDistanceSqr)
        {
            Destroy(gameObject);
            return;
        }

        transform.position += transform.up * speed * Timekeeper.DeltaTimeMain;
	}
	
	#endregion
	
	
	#region Public Methods
	
	public void Init(float speed, float distance)
    {
        this.speed = speed;
        maxDistanceSqr = distance * distance;
    }
	
	#endregion
	
}
