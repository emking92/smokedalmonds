﻿using UnityEngine;
using System.Collections;

public class PhysicsCheck : MonoBehaviour {

    public float acceleration = 4f;
    public float maxSpeed = 150f;
    public float gravity = 6f;
    public float maxfall = 200f;
    public float jump = 200f;
    public int angleLeeway = 0;

    public LayerMask layerMask;

    Rect box;

    Vector2 velocity, pos;

    bool grounded = false;
    bool falling = false;

    int horizontalRays = 6;
    int verticalRays = 4;
    float margin = .1f;

    Vector2 moveDir;
    bool willJump;

    Collider2D coll;

	// Use this for initialization
	void Start () {
        //layerMask = LayerMask.NameToLayer("normalCollisions");
        coll = GetComponent<Collider2D>();
	}

    void FixedUpdate()
    {
        box = new Rect(
            coll.bounds.min.x,
            coll.bounds.min.y,
            coll.bounds.size.x,
            coll.bounds.size.y
        );

        ApplyGravity();
        ApplyHorizontalMovement();
    }

    void CheckOverheadCollisions()
    {
        Vector2 startPoint = new Vector2(box.xMin + margin, box.center.y);
        Vector2 endPoint = new Vector2(box.xMax - margin, box.center.y);

        RaycastHit2D[] hitInfos = new RaycastHit2D[verticalRays];

        float distance = box.height / 2 + (grounded ? margin : Mathf.Abs(velocity.y * Timekeeper.DeltaTimeMain));

        for (int i = 0; i < verticalRays; i++)
        {
            float lerpAmount = (float)i / (float)(verticalRays - 1);
            Vector2 origin = Vector2.Lerp(startPoint, endPoint, lerpAmount);

            hitInfos[i] = Physics2D.Raycast(origin, Vector2.up, distance, layerMask);

            Debug.DrawRay(origin, Vector2.up, Color.green);

            if (hitInfos[i])
            {
                transform.Translate(Vector2.up * (hitInfos[i].distance - box.height / 2));
                velocity = new Vector2(velocity.x, 0);
                break;
            }
        }
    }

    void ApplyGravity()
    {
        if (!grounded)
        {
            //velocity = new Vector2(velocity.x, Mathf.Max(velocity.y - gravity, -maxfall));
        }

        if (velocity.y < 0)
        {
            falling = true;
        }
        else if (velocity.y > 0)
        {
            CheckOverheadCollisions();
        }

        if (grounded || falling)
        {
            Vector2 startPoint = new Vector2(box.xMin + margin, box.center.y);
            Vector2 endPoint = new Vector2(box.xMax - margin, box.center.y);

            RaycastHit2D[] hitInfos = new RaycastHit2D[verticalRays];

            float distance = box.height / 2 + (grounded ? margin : Mathf.Abs(velocity.y * Timekeeper.DeltaTimeMain));

            float smallestFraction = Mathf.Infinity;
            int indexUsed = 0;

            bool connected = false;

            for (int i = 0; i < verticalRays; i++)
            {
                float lerpAmount = (float)i / (float)(verticalRays - 1);
                Vector2 origin = Vector2.Lerp(startPoint, endPoint, lerpAmount);
                //Ray2D ray = new Ray2D(origin, -Vector2.up);

                if (falling)
                {
                    hitInfos[i] = Physics2D.Raycast(origin, -Vector2.up, distance, layerMask);

                    Debug.DrawRay(origin, -Vector2.up, Color.green);

                    if (hitInfos[i].fraction > 0)
                    {
                        connected = true;
                        if (hitInfos[i].fraction < smallestFraction)
                        {
                            indexUsed = i;
                            smallestFraction = hitInfos[i].fraction;
                        }
                    }
                }
            }

            if (connected)
            {
                grounded = true;
                falling = false;
                transform.Translate(-Vector2.up * (hitInfos[indexUsed].distance - box.height / 2));
                velocity = new Vector2(velocity.x, 0);
            }
            else
            {
                grounded = false;
            }
        }
    }

    void ApplyHorizontalMovement()
    {
        float horizontalAxis = moveDir.x;

        float newVelocityX = velocity.x;
        if (horizontalAxis != 0)
        {
            newVelocityX += acceleration * horizontalAxis;
            newVelocityX = Mathf.Clamp(newVelocityX, -maxSpeed, maxSpeed);
        }
        else if (velocity.x != 0)
        {
            int modifier = velocity.x > 0 ? -1 : 1;
            newVelocityX += acceleration * modifier;
        }

        velocity = new Vector2(newVelocityX, velocity.y);

        if (velocity.x != 0)
        {
            Vector2 startPoint = new Vector2(box.center.x, box.yMin + margin);
            Vector2 endPoint = new Vector2(box.center.x, box.yMax - margin);

            RaycastHit2D[] hitInfos = new RaycastHit2D[horizontalRays];
            int amountConnected = 0;
            float lastFraction = 0;

            float sideRayLength = box.width / 2 + Mathf.Abs(newVelocityX * Timekeeper.DeltaTimeMain);
            Vector2 direction = newVelocityX > 0 ? Vector2.right : -Vector2.right;
            bool connected = false;

            for (int i = 0; i < horizontalRays; i++)
            {
                float lerpAmount = (float)i / (float)(horizontalRays - 1);
                Vector2 origin = Vector2.Lerp(startPoint, endPoint, lerpAmount);

                hitInfos[i] = Physics2D.Raycast(origin, direction, sideRayLength, layerMask);

                Debug.DrawRay(origin, direction, Color.green);

                if (hitInfos[i].fraction > 0)
                {
                    connected = true;

                    if (lastFraction > 0)
                    {
                        float angle = Vector2.Angle(hitInfos[i].point - hitInfos[i - 1].point, Vector2.right);

                        if (Mathf.Abs(angle - 90) < angleLeeway)
                        {
                            transform.Translate(direction * (hitInfos[i].fraction * sideRayLength - box.width / 2));
                            velocity = new Vector2(0, velocity.y);
                            break;
                        }
                    }
                    amountConnected++;
                    lastFraction = hitInfos[i].fraction;
                }
            }
        }
    }

    void ApplyVerticalMovement()
    {
        if (grounded && willJump)
        {
            velocity = new Vector2(velocity.x, jump);
            willJump = false;
            grounded = false;
        }
    }

    void LateUpdate()
    {
        if (grounded)
            transform.Translate(new Vector2(velocity.x * Timekeeper.DeltaTimeMain, 0f));
        else
            transform.Translate(new Vector2(velocity.x*Timekeeper.DeltaTimeMain, velocity.y * Timekeeper.DeltaTimeMain + .5f * gravity * Timekeeper.DeltaTimeMain * Timekeeper.DeltaTimeMain));

        velocity.y -= gravity * Timekeeper.DeltaTimeMain;
    }
	
	// Update is called once per frame
	void Update () {
        ApplyVerticalMovement();
        
	}

    public void setMoveDirection(Vector2 dir)
    {
        /*if (dir.x > 0f && transform.localScale.x == -1 || dir.x < 0f && transform.localScale.x == 1)
        {
            gameObject.BroadcastMessage("Flip");
        }*/
        moveDir = dir.normalized;
    }

    public void setVelocity(Vector2 dir)
    {
        velocity = dir.normalized;
        moveDir = dir.normalized;
    }

    public bool setJump()
    {
        if (grounded)
        {
            willJump = true;
            return true;
        }
        return false;
    }
}
