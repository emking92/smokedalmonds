﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SmoothCamera : MonoBehaviour 
{

	#region Fields

    public float MaxDistance = 1f;
    public float FollowSpeed = 3f;

    private Vector2 Offset;
    private float z;
    private float MaxDistanceSqr;
	
	#endregion
	
	
	#region Constants
	
	
	
	#endregion
	
	
	#region Properties
	
	
	
	#endregion
	

	#region Behaviours

    private void Awake()
    {
        MaxDistanceSqr = MaxDistance * MaxDistance;
    }

    private void Start()
    {
        Offset = transform.position2D() - Player.CurrentLocation;
        z = transform.position.z;
    }

	private void LateUpdate() 
	{
        float displacementY = (transform.position2D() - Player.CurrentLocation - Offset).y;
        
        if (Mathf.Abs(displacementY) > MaxDistance)
        {
            displacementY = Mathf.Sign(displacementY) * MaxDistance;
        }
        else if (Mathf.Abs(displacementY) < .01f)
        {
            displacementY = 0;
        }
        else
        {
            displacementY = Mathf.Sign(displacementY) * Mathf.Max(0, Mathf.Abs(displacementY) - FollowSpeed * Timekeeper.DeltaTimeMain);
        }

        transform.position = (Vector3) (Player.CurrentLocation + Offset) + new Vector3(0, displacementY, z);
	}
	
	#endregion
	
	
	#region Public Methods

    public void Snap()
    {
        transform.position = (Vector3)(Player.CurrentLocation + Offset) - Vector3.forward * z;
    }
	
	#endregion
	
	
	#region Private Methods
	
	
	
	#endregion
	
}
