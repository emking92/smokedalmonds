﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ResettablePosition : MonoBehaviour, IResettable
{

	#region Fields

    private Vector3 startPosition;
	
	#endregion
	

	#region Behaviours

	private void Awake()
	{
        startPosition = transform.position;
	}

	#endregion
	
	
	#region Public Methods

    public void ResetState()
    {
        transform.position = startPosition;
    }
	
	#endregion
	
}
