﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ChunkManager : MonoBehaviour 
{

	#region Fields

    public static GameMode GameMode;

    public static int Seed;
    public GameObject TestChunk;

    public float loadDistance = 15f;
    public float unloadDistance = 10f;
    public int CheckpointInterval = 3;

    public GameObject CheckpointPrefab;
    public Vector2 CheckpointPosition = new Vector2(.5f, .5f);

    private System.Random rand;
    private GameObject[] ChunkPrefabs;
    private Queue<GameObject> Chunks;
    private Vector2 nextChunkStart;

    private Queue<float> checkpointsX;
    private int checkpointProgress;

	#endregion
	
	
	#region Constants
	
	
	
	#endregion
	
	
	#region Properties

    public static ChunkManager Current
    {
        get;
        private set;
    }
	
	#endregion
	

	#region Behaviours

	private void Awake()
	{
        Current = this;
        CheckpointInterval = (GameMode == global::GameMode.MomentLess) ? -1 : 2;

        Chunks = new Queue<GameObject>();
        checkpointsX = new Queue<float>();

        if (ChunkPrefabs == null)
        {
            ChunkPrefabs = Resources.LoadAll<GameObject>("Chunks");
        }

        rand = new System.Random(Seed);
	}
	
	private void Start() 
	{
        checkpointProgress = 1;
        Generate();
	}	
	
	private void Update() 
	{
        Generate();
	}
	
	#endregion
	
	
	#region Public Methods

    public void ResetStates()
    {
        Chunks.Where(c => c.transform.position.x +1f > Player.CurrentLocation.x)
              .Foreach(c => c.BroadcastMessage("ResetState"));
    }
	
	#endregion
	
	
	#region Private Methods

    private void Generate()
    {
        while (nextChunkStart.x < Player.CurrentLocation.x + loadDistance)
        {
            AppendChunk();
        }
        CleanChunks();
    }

    private void AppendChunk()
    {
        int r = rand.Next() % ChunkPrefabs.Length ;
        GameObject chunkPrefab = (TestChunk == null) ? ChunkPrefabs[r] : TestChunk;

        GameObject nextChunk = GameObject.Instantiate(chunkPrefab, nextChunkStart, Quaternion.identity) as GameObject;
        Chunks.Enqueue(nextChunk);
        nextChunk.GetComponentsInChildren<IRandomizable>().Foreach(c => c.Randomize(rand.Next()));

        nextChunkStart += nextChunk.GetComponent<Chunk>().Length;

        checkpointProgress--;
        if (checkpointProgress == 0)
        {
            GameObject checkpoint = GameObject.Instantiate(CheckpointPrefab, Vector2.up*100, Quaternion.identity) as GameObject;
            checkpoint.SetParentObject(nextChunk);
            checkpoint.transform.localPosition = CheckpointPosition;

            CheckpointInterval++;
            checkpointProgress = CheckpointInterval;
            checkpointsX.Enqueue(checkpoint.transform.position.x);
        }
    }

    private void CleanChunks()
    {
        while (checkpointsX.Count>1 && Player.CurrentLocation.x > checkpointsX.ElementAt(1))
        {
            checkpointsX.Dequeue();
        }

        do
        {
            if (!checkpointsX.Any()) break;

            GameObject oldestChunk = Chunks.Peek();
            float chunksRight = oldestChunk.transform.position.x + oldestChunk.GetComponent<Chunk>().Length.x;

            if (chunksRight + unloadDistance < checkpointsX.Peek())
            {
                Destroy(oldestChunk);
                Chunks.Dequeue();
            }
            else
            {
                break;
            }

        } while (true);
    }

	#endregion
	
}

public enum GameMode
{
    MomentLoss,
    MomentLess
}