﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour
{

    #region Fields

    public float MoveSpeed, JumpHeight;
    public KeyCode Up, Down, Left, Right;

    public GameObject Explosion;

    public AudioSource Death, Jump;

    public bool IsKicking, IsSliding;

    public bool IsAlive;

    private Rigidbody2D rigidbody;
    public Collider2D standingCollider, slidingCollider;
    private ColliderCheck jumpCheck;
    private float jumpTimer;
    private bool slideCheck, kickCheck;

    private Checkpoint lastCheckpoint;
    private Animator animator;

    #endregion


    #region Properties
    
    public static Player Current
    {
        private set;
        get;
    }

    public static Vector2 CurrentLocation
    {
        private set
        {
            Current.transform.position = value;
        }
        get
        {
            return Current.transform.position;
        }
    }

    public bool CanJump
    {
        get
        {
            return (!IsAirborne && !IsKicking && !IsSliding && jumpTimer <= 0);
        }
    }

    public bool CanSlide
    {
        get
        {
            return (!IsAirborne && !IsKicking);
        }
    }

    public bool CanKick
    {
        get
        {
            return (!IsAirborne && !IsSliding);
        }
    }

    public bool IsAirborne
    {
        get
        {
            return !jumpCheck.IsColliding;
        }
    }

    private void Awake()
    {
        Current = this;
        jumpCheck = GetComponentInChildren<ColliderCheck>();
        IsAlive = true;
    }

    #endregion


    #region Behaviours

    // Use this for initialization
	private void Start ()
    {
        rigidbody = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        DeathHUD.Current.gameObject.SetActive(false);
	}
	
	// Update is called once per frame
	private void Update () 
    {
        if (!IsAlive)
        {
            if (Input.GetKey(KeyCode.Space))
            {
                Respawn();
            }
            return;
        }

        CheckInput();
        transform.Translate(Vector2.right * MoveSpeed * Timekeeper.DeltaTimeMain);
        
        jumpTimer -= Timekeeper.DeltaTimeMain;

        if (CanJump)
        {
            animator.SetInteger("AnimationMode", 0);
        }
        
        if (IsAirborne && rigidbody.velocity.y < 0)
        {
            animator.SetInteger("AnimationMode", 2);
        }
	}

    #endregion


    #region Private Methods

    private void CheckInput()
    {
        if (CanJump && Input.GetKey(Up))
        {
            rigidbody.AddForce(new Vector2(0, JumpHeight));
            jumpTimer = .1f;
            animator.SetInteger("AnimationMode", 1);
            Jump.Play();
        }

        IsSliding = (CanSlide && Input.GetKey(Down));
        if (IsSliding)
        {
            animator.SetInteger("AnimationMode", 4);
            if (!slideCheck)
            {
                slideCheck = true;
                SwitchColliders();
            }
        }
        else
        {
            if (slideCheck)
            {
                slideCheck = false;
                SwitchColliders();
            }
        }
        
        IsKicking = (CanKick && Input.GetKey(Right));
        if (IsKicking)
        {
            if (!kickCheck)
            {
                kickCheck = true;
                animator.SetInteger("AnimationMode", 5);
            }
        }
        else
        {
            if (kickCheck)
            {
                kickCheck = false;
            }
        }
    }

    private void SwitchColliders()
    {
        standingCollider.enabled = !standingCollider.enabled;
        slidingCollider.enabled = !slidingCollider.enabled;
    }

    private void Respawn()
    {
        IsAlive = true;
        DeathHUD.Current.gameObject.SetActive(false);
        GetComponent<SpriteRenderer>().enabled = true;
        rigidbody.isKinematic = false;

        transform.position = lastCheckpoint.transform.position;
        ChunkManager.Current.ResetStates();
        rigidbody.velocity = Vector2.zero;

        Camera.main.SendMessage("Snap");
    }

    #endregion


    #region Public Methods

    public void Die()
    {
        if (!IsAlive) return;

        Death.Play();

        IsAlive = false;
        DeathHUD.Current.gameObject.SetActive(true);
        GetComponent<SpriteRenderer>().enabled = false;
        rigidbody.isKinematic = true;

        GameObject.Instantiate(Explosion, transform.position, transform.rotation);
    }

    public void OnCheckpoint(Checkpoint checkpoint)
    {
        lastCheckpoint = checkpoint;
    }

    #endregion
}
