﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class TitleGeneration : MonoBehaviour {
    private string title;
    private List<string> titleAdjectives, titleNouns;
    public Text text;

	// Use this for initialization
	void Start () {
        titleAdjectives = new List<string>();
        titleNouns = new List<string>();
        generateTitle();
        text.text = title;
	}

    private void generateTitle()
    {
        populateLists();
        title = "";
        for (int i = 0; i < 5; i++)
        {
            int rn = Random.Range(0, titleAdjectives.Count);
            title += " " + titleAdjectives[rn];
            titleAdjectives.RemoveAt(rn);
        }
        title += " " + titleNouns[Random.Range(0, titleNouns.Count)];
    }

    private void populateLists()
    {
        titleAdjectives.Clear();
        titleAdjectives.Add("Super");
        titleAdjectives.Add("Ultra");
        titleAdjectives.Add("Mega");
        titleAdjectives.Add("Happy");
        titleAdjectives.Add("Epic");
        titleAdjectives.Add("Gigantic");
        titleAdjectives.Add("Crazy");
        titleAdjectives.Add("Fantastic");
        titleAdjectives.Add("Splendid");
        titleAdjectives.Add("Wonderful");
        titleAdjectives.Add("Endless");
        titleAdjectives.Add("Kooky");
        titleAdjectives.Add("Marvelous");
        titleAdjectives.Add("Enchanting");
        titleAdjectives.Add("Lovely");
        titleAdjectives.Add("Pleasant");
        titleAdjectives.Add("Brilliant");
        titleAdjectives.Add("Superb");
        titleAdjectives.Add("Magnificent");
        titleAdjectives.Add("Amazing");
        titleAdjectives.Add("Awesome");
        titleAdjectives.Add("Unbelievable");
        titleAdjectives.Add("Incredible");
        titleAdjectives.Add("Terrific");
        titleAdjectives.Add("Outstanding");
        titleAdjectives.Add("Sensational");
        titleAdjectives.Add("Smashing");
        titleAdjectives.Add("Glorious");
        titleAdjectives.Add("Cool");
        titleAdjectives.Add("Neat");
        titleAdjectives.Add("Swell");
        titleAdjectives.Add("Wicked");
        titleAdjectives.Add("Excellent");
        titleAdjectives.Add("Exquisite");
        titleAdjectives.Add("Out of this World");
        titleAdjectives.Add("EXTREME");
        titleAdjectives.Add("EXPLOSIVE");
        titleAdjectives.Add("Gooder");

        titleNouns.Clear();
        titleNouns.Add("Adventure");
        titleNouns.Add("Journey");
        titleNouns.Add("Quest");
        titleNouns.Add("Trip");
        titleNouns.Add("Experience");
        titleNouns.Add("Undertaking");
        titleNouns.Add("Odyssey");
        titleNouns.Add("Trek");
        titleNouns.Add("Pilgrimage");
        titleNouns.Add("Happening");

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            generateTitle();
            text.text = title;
        }
	}
}
