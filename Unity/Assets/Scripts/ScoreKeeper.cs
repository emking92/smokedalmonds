﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScoreKeeper : MonoBehaviour {

    public Text text;

    private float startX;
    private int maxScore;

	// Use this for initialization
	void Start () {
        startX = 0;
        maxScore = 0;
	}
	
	// Update is called once per frame
	void Update () {
        text.text = GetScore().ToString();
	}

    public int GetScore()
    {
        int score = (int)(Player.CurrentLocation.x - startX);
        if (score > maxScore)
            maxScore = score;

        return maxScore;
    }
}
