﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Chunk : MonoBehaviour 
{

	#region Fields

    public Vector2 Length;
	
	#endregion
	
	
	#region Constants
	
	
	
	#endregion
	
	
	#region Properties
	
	
	
	#endregion
	

	#region Behaviours

	private void Awake()
	{
		
	}
	
	private void Start() 
	{
		
	}	
	
	private void Update() 
	{
		
	}

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.magenta;
        Gizmos.DrawLine(transform.position, transform.position2D() + Length);
    }

	#endregion
	
	
	#region Public Methods
	

	
	#endregion
	
	
	#region Private Methods
	
	
	
	#endregion
	
}
