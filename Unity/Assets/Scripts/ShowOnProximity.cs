﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class ShowOnProximity : MonoBehaviour, IResettable
{

	#region Fields

    public float proximityTrigger = .5f;

    private SpriteRenderer spriteRenderer;
    private AudioSource audio;
	
	#endregion
	
	
	#region Constants
	
	
	
	#endregion
	
	
	#region Properties
	
	
	
	#endregion
	

	#region Behaviours

	private void Awake()
	{
        spriteRenderer = GetComponent<SpriteRenderer>();
        spriteRenderer.enabled = false;

        audio = GetComponent<AudioSource>();
	}
	
	private void Start() 
	{
		
	}	
	
	private void Update() 
	{
        if (!spriteRenderer.enabled && transform.position.x < Player.CurrentLocation.x + proximityTrigger)
        {
            spriteRenderer.enabled = true;
            if (audio != null)
            {
                audio.Play();
            }
        }

        if (audio != null && audio.isPlaying)
        {
            audio.volume = 10 - Mathf.Abs(transform.position.x - Player.CurrentLocation.x)*1.5f;
        }
	}
	
	#endregion
	
	
	#region Public Methods

    public void ResetState()
    {
        spriteRenderer.enabled = false;
    }
	
	#endregion
	
	
	#region Private Methods
	
	
	
	#endregion


}
