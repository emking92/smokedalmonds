﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class DeadlyObject : MonoBehaviour 
{

	#region Behaviours

    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.tag == "Player")
        {
            collider.SendMessage("Die");
        }
    }
	
	#endregion
	
}
