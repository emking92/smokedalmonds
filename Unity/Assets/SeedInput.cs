﻿using UnityEngine;
using System.Collections;

public class SeedInput : MonoBehaviour {


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	}

    public void SetSeed(string seed)
    {
        ChunkManager.Seed = seed.GetHashCode();
    }
}
